<?php

$app->get('/paradas_autorizadas', function() use($app) {
    global $key;
    $request = $app->request;
    $token = $request->headers->get('x-token');

    if (!isset($token)) {
        $array = array();

        $array['response']['error'] = 'Unauthorized: The request requires user authentication';
        $array['response']['success']  = false;

        $app->response()->status(401);
        $app->response()->header('X-Status-Reason', $array['response']['error']);

        echo json_encode($array);

    }else if($token != $key){
        $array = array();

        $array['response']['error'] = 'Unauthorized: The request requires user authentication';
        $array['response']['success']  = false;

        $app->response()->status(401);
        $app->response()->header('X-Status-Reason', $array['response']['error']);

        echo json_encode($array);
    } else {
        $app->db->exec("set names utf8");
        $posts = $app->db->query("
        SELECT 
        `pa_id`,
        `pa_movimiento_id`,
        `pa_lugar`,
        `pa_motivo`,
        `pa_tiempo`,
        `pa_fecha_alta`,
        `pa_cliente`
        FROM `db_nav_navieros`.`paradas_autorizadas_apirest`;
        ")->fetchAll(PDO::FETCH_ASSOC);
        $array = array();
        $array["response"] = $posts;
        $app->response()->header('Content-Type','application/json');
        echo json_encode($array);
    }
});

$app->post('/paradas_autorizadas', function() use($app) {
    global $key;
    try{
        global $key;
        $request = $app->request;
        $token = $request->headers->get('x-token');
    
        if (!isset($token)) {
            $array = array();
    
            $array['response']['error'] = 'Unauthorized: The request requires user authentication';
            $array['response']['success']  = false;
    
            $app->response()->status(401);
            $app->response()->header('X-Status-Reason', $array['response']['error']);
    
            echo json_encode($array);
    
        }else if($token != $key){
            $array = array();
    
            $array['response']['error'] = 'Unauthorized: The request requires user authentication';
            $array['response']['success']  = false;
    
            $app->response()->status(401);
            $app->response()->header('X-Status-Reason', $array['response']['error']);
    
            echo json_encode($array);
        }else{

            $body = $request->getBody();
            $input = json_decode($body);
            $paradas = $input->parada;

            $total = count($paradas);
            $arreglo = array();
            for($i=0; $i<$total; $i++){

                $sql = "INSERT INTO `db_nav_navieros`.`apirest_paradas_autorizadas`
                (`id_pa`,
                `id_movimiento`,
                `lugar`,
                `motivo`,
                `tiempo`,
                `fecha_alta`)
                VALUES
                (null,
                '" . $paradas[$i]->pa_movimiento_id . "',
                '" . $paradas[$i]->pa_lugar. "',
                '" . $paradas[$i]->pa_motivo. "',
                '" . $paradas[$i]->pa_tiempo. "',
                now())";

                $app->db->query($sql);
                $lastId = $app->db->lastInsertId();
                array_push($arreglo,$lastId);
            }

            $array = array();
            $array['response']['paradas'] = $arreglo;
            $array['response']['success']  = true;
            $app->response()->header('Content-Type','application/json');
            echo json_encode($array);
        }

    } catch (Exception $e){
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());

        $array = array();
        $array['response']['id'] = 0;
        $array['response']['success']  = false;
        $app->response()->header('Content-Type','application/json');
        echo json_encode($array);
    }

});