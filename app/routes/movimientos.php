<?php

$app->get('/movimiento', function() use($app) {
    global $key;
    $request = $app->request;
    $token = $request->headers->get('x-token');

    if (!isset($token)) {
        $array = array();

        $array['response']['error'] = 'Unauthorized: The request requires user authentication';
        $array['response']['success']  = false;

        $app->response()->status(401);
        $app->response()->header('X-Status-Reason', $array['response']['error']);

        echo json_encode($array);

    }else if($token != $key){
        $array = array();

        $array['response']['error'] = 'Unauthorized: The request requires user authentication';
        $array['response']['success']  = false;

        $app->response()->status(401);
        $app->response()->header('X-Status-Reason', $array['response']['error']);

        echo json_encode($array);
    } else {
        $app->db->exec("set names utf8");
        $posts = $app->db->query("
            SELECT * FROM db_nav_navieros.movimiento_apirest;
        ")->fetchAll(PDO::FETCH_ASSOC);
        $array = array();
        $array["response"] = $posts;
        $app->response()->header('Content-Type','application/json');
        echo json_encode($array);
    }
});

$app->post('/movimiento', function() use($app) {
    try{
        global $key;
        $request = $app->request;

        $token = $request->headers->get('x-token');

        if (!isset($token)) {
            $array = array();
    
            $array['response']['error'] = 'Unauthorized: The request requires user authentication';
            $array['response']['success']  = false;
    
            $app->response()->status(401);
            $app->response()->header('X-Status-Reason', $array['response']['error']);
    
            echo json_encode($array);
    
        }else if($token != $key){
            $array = array();
    
            $array['response']['error'] = 'Unauthorized: The request requires user authentication';
            $array['response']['success']  = false;
    
            $app->response()->status(401);
            $app->response()->header('X-Status-Reason', $array['response']['error']);
    
            echo json_encode($array);
        }else{

            $body = $request->getBody();
            $input = json_decode($body);
            $movimiento = $input->movimiento[0];
            $sql1 = "INSERT INTO `db_nav_navieros`.`movimiento`
            (
            `cve_corredor`,
            `booking_ref`,
            `WOrder`,
            `cve_operador_monitorista`,
            `cve_tipo_op`,
            `cita_progM`,
            `contenedor`,
            `cve_cliente`,
            `destino`,
            `direccion`,
            `cve_trans`,
            `cve_tipoMov`,
            `status_movimiento`,
            `num_incidencias`,
            `fecha_registro`,
            `notaMov`,
            `OTD`,
            `reaccion`,
            `comentario_revision`,
            `fecha_revision`,
            `tipo_gps`)
            VALUES
            (
            '" . $movimiento->mov_corredor_id . "',
            '" . $movimiento->mov_referencia_control_interno . "',
            '" . $movimiento->mov_cliente . "',
            0,
            '" . $movimiento->mov_tipo_operacion . "',
            '" . $movimiento->mov_cita_programada . "',
            '" . $movimiento->mov_contenedor . "',
            9999,
            '" . $movimiento->mov_destino_direccion . "',
            '" . $movimiento->mov_destino_direccion . "',
            1853,
            1,
            1,
            0,
            now(),
            '" . $movimiento->mov_instrucciones . "',
            '99',
            0,
            '" . $movimiento->mov_observaciones . "',
            null,
            '" . $movimiento->mov_tipo_gps . "'
            )
            ";

            $app->db->exec("set names utf8");
            $app->db->query($sql1);
            $id_movimiento = $app->db->lastInsertId();

            $sql2 = "INSERT INTO `db_nav_navieros`.`apirest_movimiento`
            (`id_movimiento`,
            `transportista_linea`,
            `transportista_contacto`,
            `transportista_telefono`,
            `transportista_email`,
            `colocacion_fecha`,
            `colocacion_lugar`,
            `origen_direccion`,
            `destino_direccion`,
            `contenedor_tipo`,
            `contenedor_id`,
            `modalidad`,
            `instrucciones`,
            `observaciones`,
            `patente`,
            `referencia_control_interno`,
            `cliente_nombre`,
            `gps_tipo`,
            `cita_programada`,
            `tipo_operacion`,
            `corredor_id`)
            VALUES
            ('" . $id_movimiento . "',
            '" . $movimiento->mov_trans_linea . "',
            '" . $movimiento->mov_trans_contacto . "',
            '" . $movimiento->mov_trans_telefono . "',
            '" . $movimiento->mov_trans_email . "',
            '" . $movimiento->mov_colocacion_fecha . "',
            '" . $movimiento->mov_colocacion_lugar . "',
            '" . $movimiento->mov_origen_direccion . "',
            '" . $movimiento->mov_destino_direccion . "',
            '" . $movimiento->mov_contenedor_tipo . "',
            '" . $movimiento->mov_contenedor . "',
            '" . $movimiento->mov_modalidad . "',
            '" . $movimiento->mov_instrucciones . "',
            '" . $movimiento->mov_observaciones . "',
            '" . $movimiento->mov_patente . "',
            '" . $movimiento->mov_referencia_control_interno . "',
            '" . $movimiento->mov_cliente . "',
            '" . $movimiento->mov_tipo_gps . "',
            '" . $movimiento->mov_cita_programada . "',
            '" . $movimiento->mov_tipo_operacion . "',
            '" . $movimiento->mov_corredor_id . "');";

            $app->db->query($sql2);
            $lastId = $app->db->lastInsertId();

            if( !empty($movimiento->mov_contacto_1) ){
                $sql3 = "INSERT INTO `db_nav_navieros`.`apirest_cliente`
                (`id_cliente`,
                `id_movimiento`,
                `contacto_nombre`,
                `contacto_telefono`,
                `contacto_email`)
                VALUES
                (null,
                '" . $id_movimiento . "',
                '" . $movimiento->mov_contacto_1 . "',
                '" . $movimiento->mov_contacto_telefono_1 . "',
                '" . $movimiento->mov_contacto_email_1 . "');";
                $app->db->query($sql3);
            }

            if( !empty($movimiento->mov_contacto_2) ){
                $sql4 = "INSERT INTO `db_nav_navieros`.`apirest_cliente`
                (`id_cliente`,
                `id_movimiento`,
                `contacto_nombre`,
                `contacto_telefono`,
                `contacto_email`)
                VALUES
                (null,
                '" . $id_movimiento . "',
                '" . $movimiento->mov_contacto_2 . "',
                '" . $movimiento->mov_contacto_telefono_2 . "',
                '" . $movimiento->mov_contacto_email_2 . "');";
                $app->db->query($sql4);
            }

            if( !empty($movimiento->mov_contacto_3) ){
                $sql5 = "INSERT INTO `db_nav_navieros`.`apirest_cliente`
                (`id_cliente`,
                `id_movimiento`,
                `contacto_nombre`,
                `contacto_telefono`,
                `contacto_email`)
                VALUES
                (null,
                '" . $id_movimiento . "',
                '" . $movimiento->mov_contacto_3 . "',
                '" . $movimiento->mov_contacto_telefono_3 . "',
                '" . $movimiento->mov_contacto_email_3 . "');";
                $app->db->query($sql5);
            }

            $array = array();
            $array['response']['id'] = $lastId;
            $array['response']['success']  = true;
            $app->response()->header('Content-Type','application/json');
            echo json_encode($array);
        }

    } catch (Exception $e){
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());

        $array = array();
        $array['response']['id'] = 0;
        $array['response']['success']  = false;
        $app->response()->header('Content-Type','application/json');
        echo json_encode($array);
    
    }

});