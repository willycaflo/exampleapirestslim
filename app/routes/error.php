<?php

$app->map('/unauthorized', function() use($app) {

    $array = array();

    $array['response']['error'] = 'Unauthorized: The request requires user authentication';
    $array['response']['success']  = false;

    $app->response()->status(401);
    $app->response()->header('X-Status-Reason', $array['response']['error']);

    echo json_encode($array);

})->name('unauthorized');